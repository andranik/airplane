var airplane = angular.module('airplane',['ngRoute'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/administrative',{
                controller: 'mainCtrl',
                templateUrl: 'administrative.html'
            })
            .when('/create-flight',{
                controller: 'mainCtrl',
                templateUrl: 'create-flight.html'
            })
            .when('/welcome',{
                controller: 'mainCtrl',
                templateUrl: 'welcome.html'
            })
            .otherwise({ redirectTo: '/welcome' })
    })
    .controller('mainCtrl',mainCtrl)
    .factory('mainService',function(){
        var factory = {},
        flights = [],
        airplans = [],
        passengers = [
            {
                name: "Andrnaik",
                age: 27
            },
            {
                name: "Haka",
                age: 26
            },
            {
                name: "Armine",
                age: 24
            },
            {
                name: "Emma",
                age: 27
            }
        ];
        factory.getAirplans = function () {
            return airplans;
        };
        factory.getPassengers = function () {
            return passengers;
        };
        factory.getFlights = function () {
            return flights;
        };
        return factory;
    });

function mainCtrl($scope,mainService) {
    function init() {
        $scope.passengers = mainService.getPassengers();
        $scope.airplans = mainService.getAirplans();
        $scope.flights = mainService.getFlights();
    }
    init();

    function Passenger(name,age) {
        this.name = name;
        this.age = age;
        return this;
    }
    function Airplane(name,seats) {
        this.name = name;
        this.seats = seats;
        return this;
    }
    function Flight(name,distance) {
        this.name = name;
        this.distance = distance;
        return this;
    }



    $scope.addPassenger = function () {
        var obj = new Passenger($scope.passenger.name,$scope.passenger.age);
        $scope.passengers.push(obj);
    };
    $scope.removePassenger = function (indx) {
        $scope.passengers.splice(indx,1);
    };
    $scope.addToAirplane = function () {

    };
    $scope.removeFromAirplane = function () {

    };
    $scope.addAirplane = function () {
        var obj = new Airplane($scope.airplane.name,$scope.airplane.seats);
        $scope.airplans.push(obj);
    };
    $scope.removeAirplane = function (indx) {
        $scope.airplans.splice(indx,1);
    };
    $scope.addFlight = function () {
        var obj = new Flight($scope.flight.name,$scope.flight.distance);
        $scope.flights.push(obj);
    };
    $scope.removeFlight = function (indx) {
        $scope.flights.splice(indx,1);
    };
    $scope.addAirplaneToFlight = function () {

    };
    $scope.removeAirplaneFromFlight = function () {

    };

}

